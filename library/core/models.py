from django.core.validators import MinLengthValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Author(models.Model):
    """ Author class """
    first_name = models.CharField(_("First name"), max_length=128, validators=[MinLengthValidator(2)])
    last_name = models.CharField(_("Last name"), max_length=128, validators=[MinLengthValidator(2)])

    def name(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        ordering = ['last_name', 'first_name', 'id']
        verbose_name = _("Author")
        verbose_name_plural = _("Authors")
